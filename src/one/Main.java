package one;

public class Main {
	//1 Subject
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Car car1 = new Car();
		Car car2 = new Car("Honda");
		Vehicle car3 = new Car();
		Vehicle car4 = new Car("Honda");
		
		System.out.println(car1.getCar());
		System.out.println(car2.getCar());
		System.out.println(car3.getCar());
		System.out.println(car4.getCar());
	}

}
