package one;

public class Vehicle {
	String brand;
	
	public Vehicle() {
		this.brand = "New Car" ;
	}
	
	public Vehicle(String brand) {
		this.brand = brand;
	}
	
	public String getCar() {
		return brand;
	}
	
}
